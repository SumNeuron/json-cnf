import * as config from '../config/conjunctive-normal-form.js';


/**
 * Group an array of filters such that each sub-array starts with a logical "and" and all other filters are logical "or".
 * @param {array} filters - a list of filter objects
 */
export function groupByConjunctiveNormalForm(filters) {
  let grouped = [];

  // for each filter
  for (let i = 0; i < filters.length; i++) {
    let logic = filters[i].logic;

    // logical "and" marks the beginning of a new sublist
    if (logic === "and") {
      grouped.push([filters[i]]);
      continue
    } else {
      // dealing with a logical "or", will be added to latest grouping
      let list = grouped[grouped.length - 1];

      // somehow, no logical "and" anywhere
      if (typeof list === "undefined") {
        // coerce logical "or" to logical "and"
        filters[i].logic = "and";
        grouped.push([filters[i]]);
      } else {
        grouped[grouped.length - 1].push(filters[i]);
      }
    }
  }
  return grouped;
}



const get = (which, filter, config) => {
  // lookup the standard way of transforming either functions / condifionals
  let attr = config[`${which}s`][filter[which]]
  // extract the transformation
  let func = attr[which];
  // added to allow users to handle field dependent exceptions
  if (attr.exceptions && filter.field in attr.exceptions) {
    return attr.exceptions[filter.field]
  }
  return func
}

const extract = (record, field, config) => {
  // added to allow users to move slightly away from SQL-like json
  return (field in config.extractors)
    ? config.extractors[field](record[field])
    : record[field]
}



/**
 * Filter a SQL-eqsue JSON object by conjunctive normal form filters
 * @param {object} json - an object of {id: record} pairs
 * @param {array} filters - a list of filter objects
 */
export function filterJSON(json, filters, config=config) {
  let conjunctiveNormalForm = groupByConjunctiveNormalForm(filters);
  let ids = Object.keys(json);

  let filtered = ids.filter(id => {
    // extract current record
    let record = json[id];
    // calculate the truthiness of all logical "and" statements
    let and = conjunctiveNormalForm.map(groupedOrs => {
      // calculate the truthiness of all logical "or" statements
      let or = groupedOrs.some(filter => {

        // extract the function to apply
        let transform = get('function', filter, config)

        // extract the conditional to test
        let compare   = get('conditional', filter, config)

        // get the current record's field to test against
        let prop      = extract(record, filter.field, config);

        // apply the transform to the field
        let value     = transform(prop);

        // get the truthiness of comparison
        let result    =  compare(value, filter.input);
        return result
      });
      return or;
    });
    return and.every(e => e === true);
  });
  return filtered;
}
