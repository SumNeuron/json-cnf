import {
  groupByConjunctiveNormalForm,
  filterJSON
} from './utils/conjunctive-normal-form.js'

import * as config from './config/conjunctive-normal-form.js'

let jsoncnf = {
  groupByConjunctiveNormalForm,
  filterJSON,
  config
}


export {
  groupByConjunctiveNormalForm,
  filterJSON,
  config
}
export default jsoncnf
