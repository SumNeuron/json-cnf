# JSON-CNF
What is CNF?

CNF is short for Conjunctive Normal Form, which is a way of arranging logical arguments into groups of logical OR statements between AND statements, e.g.

```
(statement_{1,1} OR, .... statement_{1,i})
AND
(statement_{2,1} OR, .... statement_{2,j})
AND
...
AND
(statement_{x,1} OR, .... statement_{x,k})
```

Assuming your JSON data is in an SQL-like format:

```
{
  recordID_1: {
    column1: value,
    column2: value,
    ...,
    columnN: value
  },

  ...,

  recordID_M: {
    column1: value,
    column2: value,
    ...,
    columnN: value
  }
}
```

This library allows you to easily apply a list of filters
in CNF to your JSON to find which records pass the filters.



There are two main functions:
- groupByConjunctiveNormalForm: takes a list of filters and returns arranges them in CNF
- filterJSON: takes your JSON and a list of filters and applies the CNF grouping of those filters to the JSON



## What's a filter

A filter consists of five parts:

1. logic: AND or OR (defaults to AND)
2. function: applied to the value of the field prior to the comparisons (defaults to the identity function)
3. conditional: a logical test (defaults to equality)
4. field: a field in your records
5. input: what the value should be compared to


thus a filter is applied to a record as so:

```
conditional(function(record[field]), input)
```
